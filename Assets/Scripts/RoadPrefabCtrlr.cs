﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadPrefabCtrlr : MonoBehaviour
{
    public Transform m_cylinders;
    // Start is called before the first frame update
    float m_rotSpeed = 1;
    float m_rotRatio = 25f;
    public Transform m_cylindre;
    Transform m_rings;
    
    float m_roadLength;
    void Start()
    {
        int minObstacles = GameCtrlr.instance.m_minObstacles;
        int maxObstacles = GameCtrlr.instance.m_maxObstacles;
        int ringsSpacing = GameCtrlr.instance.m_ringsSpacing;
        m_rotSpeed       = GameCtrlr.instance.m_cylindreRotSpeed;  

        m_rings = GetRings();      
        // Defining Rings
        // DefineRings(ringsSpacing);
        // DefineObstacles(minObstacles, maxObstacles);

    }
    void ActivateShape()
    {
        
    }

    
    // Update is called once per frame
    void FixedUpdate()
    {
        if(!GameCtrlr.instance.IsGameOver() && !GameCtrlr.instance.IsGamePaused())
        {   
            float rotation = (m_rotRatio * Time.fixedDeltaTime);
            // m_rings.Rotate(0f,(rotation * m_rotSpeed),  0.0f, Space.Self);
        }

    }




    void DefineObstacles(int _min, int _max)
    {
        foreach (Transform ring in m_rings)
        {
            RingSectionCtrlr ringSectionCtrlr = ring.GetComponent<RingSectionCtrlr>();

            int randomObstaclesNumber = Random.Range(_min, _max);
            ringSectionCtrlr.DefineObstacles(randomObstaclesNumber); 
        }
    }
    public void DestroyRoad()
    {
        Destroy(gameObject);
    }

    Transform GetActiveShape()
    {
        Transform _shape = m_cylinders.GetChild(0);
        foreach(Transform shape in m_cylinders)
        {
            if(shape.gameObject.activeInHierarchy)
            {
                _shape = shape;
                break;
            }
        }
        return _shape;
    }
    
    Transform GetRings()
    {
        Transform activeShape = GetActiveShape();
        
        return activeShape;
    }
    public Vector3 GetEndPos()
    {
        Transform activeShape = GetActiveShape();
        Transform shapeEnd = activeShape.Find("end");
        Vector3 pos = shapeEnd.position;
        return pos;
    }
    public int GetNextShapeIndex()
    {
        Transform activeShape = GetActiveShape();
        Transform nextShape = activeShape.GetComponent<ShapeCtrlr>().GetRandomShape();
        return nextShape.GetSiblingIndex();
    }
    
    
    public float SetActiveShape(int index, Vector3 pos)
    {
        foreach (Transform shape in m_cylinders)
        {
            shape.gameObject.SetActive(false);
        }

        Transform newShape = m_cylinders.GetChild(index);
        
        newShape.GetComponent<ShapeCtrlr>().SetPosition(pos);
        newShape.GetComponent<ShapeCtrlr>().DefineRings();

        Vector3 v = newShape.eulerAngles;
        newShape.eulerAngles = new Vector3(v.x, (v.y + 180), v.z);

        newShape.gameObject.SetActive(true);

        return newShape.eulerAngles.y;
    }

}
