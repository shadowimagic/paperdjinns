﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CharacterCtrlr : MonoBehaviour
{
    public GameObject m_bulletPrefab;
    public Transform m_bulletContainer;
    public TextMeshProUGUI m_coinsText;
    public TextMeshProUGUI m_gemsText;
    public TextMeshProUGUI m_livesText;
    public TextMeshProUGUI m_levelText;
    public TextMeshProUGUI m_levelReachedText;
    public GameObject m_playerItem;

    public Rigidbody m_rb;
    
    Vector3 m_forward = new Vector3(0, 0, 1);
    Vector3 m_camForward = new Vector3(0, 0, 1);

    public float m_moveSpeed = 17;

    const float m_translation = 300f;
    const float m_speed = 2f;
    float m_motion = 0f;
    float m_motionDir = 0;
    GameObject m_character;
    Camera m_cam;
    float m_camXOffset = 0f;
    float m_bulletSpeed = 2;
    float m_shootRate = 5;
    float m_lastShooted;
    bool m_shoot = false;

    public int m_coins = 0;
    public int m_gems = 0;
    public int m_lives = 5;
    
    private Vector2 m_lastPosition;
    
    int m_pos = 0;
    int m_bulletLives = 1;

    bool m_turnLerp = false;
    bool m_turnCameraLerp = false;
    enum ORIENTATION
    {
        STRAIGHT = 1,
        TURN = 2
    }

    ORIENTATION m_orientation = ORIENTATION.STRAIGHT;
    int m_orientationSide = 1;
    float m_currentOrientationAngle = 0;
    // Start is called before the first frame update
    void Start()
    {
        m_cam = Camera.main;
        m_camXOffset = m_cam.transform.position.z - transform.position.z;

        m_motion = (m_translation * m_speed);
        m_character = transform.GetChild(0).gameObject;


        // Init
        m_coins = PlayerPrefs.GetInt("coins",0);
        m_gems = PlayerPrefs.GetInt("gems",0);
        m_lives = PlayerPrefs.GetInt("lives", 5);
        if(m_lives == 0)
        {
            m_lives = 5;
            PlayerPrefs.SetInt("lives", m_lives);
        }

        m_coinsText.text = m_coins.ToString();
        m_gemsText.text = m_gems.ToString();
        m_livesText.text = m_lives.ToString();
        m_levelText.text = GameCtrlr.instance.m_currentLevel.ToString();

        // Get Bullet lives
        m_bulletLives = GameCtrlr.instance.m_gunType;

    }


    public void GenerateBullet()
    {
        GameObject instantiatedBullet = Instantiate(m_bulletPrefab, m_bulletContainer.position, m_bulletPrefab.transform.rotation);
        Material currentMaterial = GameCtrlr.instance.m_currentMaterial;
        instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetAngle(m_currentOrientationAngle);
        instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetMaterial(currentMaterial);
        instantiatedBullet.transform.GetComponent<BulletCtrlr>().SetLives(m_bulletLives);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!GameCtrlr.instance.IsGameOver() && !GameCtrlr.instance.IsGamePaused())
        {
 
            m_rb.transform.Translate(m_forward * m_moveSpeed * Time.deltaTime);
            
            if(m_turnLerp)
            {
                this.RotatePlayerLerp();
            }
 

        }
        
    }

    private void Update() {
        
        if(!GameCtrlr.instance.IsGameOver() && !GameCtrlr.instance.IsGamePaused())
        {    
            // Change Color : For Test with Keyboard
            if (Input.GetKeyDown("space"))
            {
                GameCtrlr.instance.ChangeBulletColor();
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                GotoLeft();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                GotoRight();
            }

        }


        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Began)
            {
                m_lastPosition = touch.position;
            }
            if(touch.phase == TouchPhase.Moved)
            {
                // get the moved direction compared to the initial touch position
                Vector2 direction = touch.position - m_lastPosition ;

                // get the signed x direction
                if(direction.x >= 0) 
                {
                    GotoRight();
                }
                else
                {
                    GotoLeft();
                }

                float signedDirection = Mathf.Sign(direction.x);

                m_lastPosition = touch.position;
            }
        }
    }

    void GotoRight()
    {
        if(m_pos >= -1 && m_pos < 1)
        {
            StartCoroutine(ISwipe(1, 8.0f));
            m_pos += 1;
        }
    }

    void GotoLeft()
    {
        if(m_pos <= 1 && m_pos > -1)
        {
            StartCoroutine(ISwipe(-1, 8.0f));
            m_pos -= 1;
        }
    }


    IEnumerator ISwipe(int _dir = 1, float _max = 8.0f)
    {

       float progress = 0;

       while (progress < _max )
       {
            progress += (1.0f);

            if(_dir == 1)
                m_rb.transform.Translate(Vector3.right * progress);
            
            if(_dir == -1)
                m_rb.transform.Translate(Vector3.left * progress);
            

            if (progress >= _max )
            {
                break;
            }

            yield return 0;
       }

    }
     

    public void GetCoin(int type , int point) // 1 for Coin, 2 for Gem
    {
        switch (type)
        {
            case 1 : 
                m_coins += point;
                m_coinsText.text = m_coins.ToString();
                PlayerPrefs.SetInt("coins", m_coins);
            break;
            case 2 : 
                m_gems += point;
                m_gemsText.text = m_gems.ToString();
                PlayerPrefs.SetInt("gems", m_gems);
            break;
            case 3 : 
                if(m_lives < 5)
                {
                    m_lives++;
                    m_livesText.text = m_lives.ToString();
                    PlayerPrefs.SetInt("lives", m_lives);
                }
                
            break;
            case 4 :  // Star
                GameCtrlr.instance.StartStarPowerup();
            break;
            default:break;
        }
        
    }

    public void DecreasePlayerLives()
    {
        StartCoroutine(IInjureEffect());

        m_lives--;
        if(m_lives > 0)
        {
            m_livesText.text = m_lives.ToString(); 
        }
        else
        {
            m_livesText.text = "0"; 
            // GameOver
            StartCoroutine(IWaitBeforeGameOver());
        }

    }

    public void SaveScore()
    {
        PlayerPrefs.SetInt("coins", m_coins);
        PlayerPrefs.SetInt("gems", m_gems);
    }

    public void NextLevel(int lvl)
    {
        m_levelText.text = lvl.ToString();
        m_levelReachedText.text = lvl.ToString();
    }

    public void TurnPlayer(float angle)
    {
        float angleDiff = m_currentOrientationAngle + angle;
        int side = angleDiff > 0 ? -1 : 1;

        ORIENTATION newOrientation = m_orientation == ORIENTATION.STRAIGHT ? ORIENTATION.TURN : ORIENTATION.STRAIGHT;
        m_orientation = newOrientation;
        m_orientationSide = side;
        m_currentOrientationAngle = angleDiff;
        // Debug.Log("rota : " + angleDiff);

        // RotatePlayer(angleDiff, side);
        StartCoroutine(IToggleTurnLerp());
        // RotatePlayerLerp();
        
    }

    void RotatePlayerLerp()
    {
        Quaternion initRotation = m_rb.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        
        if(Mathf.Abs(m_rb.rotation.y) >= Mathf.Abs(m_currentOrientationAngle) )
            m_turnLerp = false;

        m_rb.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);

    }

    void SwipePlayerLerp()
    {
        Quaternion initRotation = m_rb.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        
        if(Mathf.Abs(m_rb.rotation.y) >= Mathf.Abs(m_currentOrientationAngle) )
            m_turnLerp = false;

        m_rb.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);

    }

   void RotateCameraLerp()
    {
        Quaternion initRotation = m_cam.transform.parent.rotation;
        Vector3 destRotation = new Vector3(0, m_currentOrientationAngle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        
        if(Mathf.Abs(m_cam.transform.parent.rotation.y) >= Mathf.Abs(m_currentOrientationAngle) )
            m_turnCameraLerp = false;

        m_cam.transform.parent.rotation = Quaternion.Lerp(initRotation, finalRotation, Time.deltaTime * 5f);
    }

    IEnumerator IToggleTurnLerp()
    {
        m_turnLerp = true;
        m_turnCameraLerp = true;
        yield return 0;
    }
    IEnumerator IInjureEffect()
    {
        GameCtrlr.instance.m_isCharacterBlinking = true;

        m_playerItem.transform.GetComponent<BoxCollider>().enabled = false;

        GameCtrlr.instance.ToggleMotionOnInjured(true); // Stop all motions

        int blinks = 3;
        
        StartCoroutine(IBlink(blinks));

        yield return 0;
    }
    IEnumerator IBlink(int _blinks)
    {   
        float blinkDuration = 0.15f;
        m_playerItem.SetActive(false);
        yield return new WaitForSeconds(blinkDuration);
        m_playerItem.SetActive(true);
        yield return new WaitForSeconds(blinkDuration);
        _blinks--;
        if(_blinks > 0)
        {
            StartCoroutine(IBlink(_blinks));
        }
        else
        {
            RestartAfterInjury();
        }
    }

    void RestartAfterInjury()
    {
        // Debug.Log("Restarted...");
        m_playerItem.transform.GetComponent<BoxCollider>().enabled = true;

        GameCtrlr.instance.ToggleMotionOnInjured(false); // Restart all motions

        GameCtrlr.instance.m_isCharacterBlinking = false;

    }
    IEnumerator IWaitBeforeGameOver()
    {
        yield return new WaitForSeconds(0.5f);
        GameCtrlr.instance.GameOver();
    }
    
}
