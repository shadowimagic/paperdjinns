﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSectionCtrlr : MonoBehaviour
{
    // Start is called before the first frame update

    float m_rotSpeed = 1;
    float m_rotRatio = 25f;
    public Transform m_cylindre;
    public Transform m_rings;
    
    float m_roadLength;
    
    void Start()
    {
        int minObstacles = GameCtrlr.instance.m_minObstacles;
        int maxObstacles = GameCtrlr.instance.m_maxObstacles;
        int ringsSpacing = GameCtrlr.instance.m_ringsSpacing;
        m_rotSpeed       = GameCtrlr.instance.m_cylindreRotSpeed;        
        // Defining Rings
        DefineRings(ringsSpacing);
        DefineObstacles(minObstacles, maxObstacles);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!GameCtrlr.instance.IsGameOver() && !GameCtrlr.instance.IsGamePaused())
        {   
            float rotation = (m_rotRatio * Time.fixedDeltaTime);
            // m_rings.Rotate(0f,(rotation * m_rotSpeed),  0.0f, Space.Self);
        }

    }

    void DefineRings(int _ringsSpacing)
    {
        // Hide all rings
        foreach (Transform ring in m_rings)
        {
            ring.gameObject.SetActive(false);
        }
        // Activate only needed ones
        // int realDivider = (_ringsSpacing + 1); //  
        int lastRingIndex = 0;
        m_rings.GetChild(lastRingIndex).gameObject.SetActive(true);

        foreach (Transform ring in m_rings)
        {
            int ringIndex = ring.GetSiblingIndex();

            // if((ringIndex%realDivider) == _ringsSpacing)
            // {
            //     ring.gameObject.SetActive(true);
            // }
            if(ringIndex == (lastRingIndex + _ringsSpacing + 1))
            {
                lastRingIndex = ringIndex;
                ring.gameObject.SetActive(true);
            }
        }


    }


    void DefineObstacles(int _min, int _max)
    {
        foreach (Transform ring in m_rings)
        {
            RingSectionCtrlr ringSectionCtrlr = ring.GetComponent<RingSectionCtrlr>();

            int randomObstaclesNumber = Random.Range(_min, _max);
            ringSectionCtrlr.DefineObstacles(randomObstaclesNumber); 
        }
    }
    public void DestroyRoad()
    {
        Destroy(gameObject);
    }
}
