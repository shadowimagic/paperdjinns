﻿using System.Collections.Generic;
using UnityEngine;
public class ShapeCtrlr : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform m_cylinders;
    public List<Transform> m_linkedShapes;
    Transform m_start;
    Transform m_end;
    void Start()
    {
        m_start = transform.Find("start");
        m_end = transform.Find("end");


        //Initial defintion
        DefineRings();

    }
    public Transform GetRandomShape()
    {
        int r = Random.Range(0, m_linkedShapes.Count);
        return m_linkedShapes[r];
    }

    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }

    public void DefineRings()
    {
        // Get current rings spacing and obsts specs from GameCtrlr script
        int _ringsSpacing = GameCtrlr.instance.m_ringsSpacing;
        int minObstacles = GameCtrlr.instance.m_minObstacles;
        int maxObstacles = GameCtrlr.instance.m_maxObstacles;

        // Define activables indexes
        int[] activablesIndex = new int[]{0, 1, 2};
        switch (_ringsSpacing)
        {
           case 1: activablesIndex = new int[]{0,1,2}; break;
           case 2: activablesIndex = new int[]{0,2}; break;
           case 3: activablesIndex = new int[]{1}; break;
        }

        // Get each cylinder
        foreach (Transform cylinder in m_cylinders)
        {
            // Get cylinder's rings
            Transform rings = cylinder.Find("Rings");
            if(rings.childCount > 0)
            {    // Hide all rings
                foreach (Transform ring in rings)
                {
                    ring.gameObject.SetActive(false);
                }
                // Activate only indexes in activables
                foreach (int ringIndex in activablesIndex)
                {
                    Transform ring = rings.GetChild(ringIndex);
                    ring.gameObject.SetActive(true);

                    // Define Obstacles (Number and Appearence)
                    int randomObstaclesNumber = Random.Range(minObstacles, maxObstacles);
                    ring.GetComponent<RingSectionCtrlr>().DefineObstacles(randomObstaclesNumber);
                    
                }
            }
        
        }

    }
    
}
