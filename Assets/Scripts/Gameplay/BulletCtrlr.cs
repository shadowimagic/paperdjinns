using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCtrlr : MonoBehaviour
{
    public int m_lives = 1;
    const float m_translation = 300f;
    const float m_speed = 30f;
    float m_motion = 0f;
    float m_motionz = 0;
    GameObject m_character;
    float m_bulletBaseSpeed =   1200;
    float m_bulletSpeed     =   1.5f;
    bool  m_isDestroyed     =   false;
    float m_timeToLive      =   0.5f;
    // Start is called before the first frame update
    void Start()
    {
        // transform.GetComponent<Rigidbody>().velocity = Vector3.forward * (m_bulletBaseSpeed * m_bulletSpeed);
        Destroy(gameObject, m_timeToLive);
    }

    public void SetAngle(float angle)
    {   
        Rigidbody rb = transform.GetComponent<Rigidbody>();

        Vector3 destRotation = new Vector3(0, angle, 0) ;//* (float)dir;
        Quaternion finalRotation = Quaternion.Euler(destRotation);
        rb.rotation = finalRotation;
    }
    public void SetLives(int lives)
    {
        m_lives = lives;
    }
    public void SetMaterial(Material mat)
    {
        string materialName = mat.name;
        string[] splitArray =  materialName.Split(char.Parse("_")); //
        string tag = splitArray[1];
        Transform bulletMesh = transform.GetChild(0);
        transform.gameObject.tag = tag;
        bulletMesh.gameObject.tag = tag;
        bulletMesh.GetComponent<MeshRenderer>().sharedMaterial = mat;
    }

    void FixedUpdate()
    {
       transform.Translate(Vector3.forward * m_speed);
    }
    

}


