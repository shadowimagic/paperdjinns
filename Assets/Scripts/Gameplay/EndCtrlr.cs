﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCtrlr : MonoBehaviour
{
    public Transform m_road;
    IEnumerator IDestroyRoad()
    {
        yield return new WaitForSeconds(1.0f);
        // destroy current road
        DestroyRoad();
    }
    
    public void DestroyRoad()
    {
        // Destroy previous
        m_road.GetComponent<RoadPrefabCtrlr>().DestroyRoad();
        // Generate next road
        FindObjectOfType<TestGenerate>().GenerateRoadShape();

    }

    void OnTriggerExit(Collider collision)
    {   
        if(collision.gameObject.tag == "Player")
        {
            // Debug.Log("Destroying");
            StartCoroutine(IDestroyRoad());
        }
    }


}
