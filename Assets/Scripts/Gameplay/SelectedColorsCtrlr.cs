﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedColorsCtrlr : MonoBehaviour
{
    public Transform m_colorsBoxContainer;
    public GameObject m_colorBoxPrefab;

    int m_activateColorIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    public void DefineBoxes(List<Material> colorsMaterials)
    {
        foreach (Transform item in m_colorsBoxContainer)
        {
            Destroy(item.gameObject);
        }

        for (int i = 0; i < colorsMaterials.Count; i++)
        {
            GameObject instantiatedColorBox = Instantiate(m_colorBoxPrefab, Vector3.zero, Quaternion.identity, m_colorsBoxContainer);
            DefineColor(instantiatedColorBox, colorsMaterials[i]);
        }

        // Activate color
        ActivateColor(m_activateColorIndex);
    }

    void DefineColor(GameObject colorBox, Material material)
    {
        Color c = material.color;
        colorBox.transform.GetChild(0).GetComponent<Image>().color = c;
    }

    public void ActivateColor(int index)
    {
        // Deactivate prev 
        m_colorsBoxContainer.GetChild(m_activateColorIndex).GetComponent<Animation>().Play("colorSelectionUnselected");
        // Activate new
        m_colorsBoxContainer.GetChild(index).GetComponent<Animation>().Play("colorSelectionSelected");
        m_activateColorIndex = index;

    }

}
