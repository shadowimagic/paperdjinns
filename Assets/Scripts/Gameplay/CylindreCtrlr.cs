﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylindreCtrlr : MonoBehaviour
{

    bool m_didEnteredRoad = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // void OnCollisionEnter(Collision collision)
    // {
    //     Debug.Log("Wuy izion");
    //     // m_didEnteredRoad = true;
    // }

    // void OnCollisionExit(Collision collision)
    // {
    //     Debug.Log("Wuy exit");
    //     if(!GameCtrlr.instance.m_isCharacterBlinking)
    //         StartCoroutine(IDestroyRoad());
    // }

    IEnumerator IDestroyRoad()
    {
        yield return new WaitForSeconds(1.2f);
        // destroy current road
        DestroyRoad();


    }
    public void DestroyRoad()
    {
        transform.parent.GetComponent<RoadSectionCtrlr>().DestroyRoad();

        // Generate a new one
        FindObjectOfType<RoadGenerationCtrlr>().GenerateRoad();
    }
}
