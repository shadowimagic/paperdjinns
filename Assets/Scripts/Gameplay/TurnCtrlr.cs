﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnCtrlr : MonoBehaviour
{
    // Start is called before the first frame update
    public float m_angle = 90.0f;
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider collision)
    {

        if(collision.gameObject.tag == "Player")
        {
            Transform Player = collision.transform.parent; // Character is inside Player (which contains the CharacterCtrlr script)
            Player.GetComponent<CharacterCtrlr>().TurnPlayer(m_angle);
        }
    }

   
}
