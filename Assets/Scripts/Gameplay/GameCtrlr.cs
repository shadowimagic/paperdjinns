﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using TMPro;


public class GameCtrlr : MonoBehaviour
{
    public static GameCtrlr instance;
    
    public CharacterCtrlr m_characterCtrlr;
    public float m_cylindreRotSpeed = 0.5f;

    public GameObject m_selectionColorsBox;

    public AudioSource m_audioSource;

    public AudioClip m_levelupSound;
    public AudioClip m_gameoverSound;

    const float m_BASE_ROTATION_SPEED = 2f;
    bool m_gameOver = true;
    bool m_gamePaused = true;

    [HideInInspector]
    public int m_minObstacles = 1;
    [HideInInspector]
    public int m_maxObstacles = 3;
    [HideInInspector]
    public int m_ringsSpacing = 2; // 0:1:2 (0, 1 or 2 rings unit between two rings)
    // Start is called before the first frame update
    [HideInInspector]
    public int m_maxColors = 2; 
    
    public List<Material> m_obstaclesMaterials = new List<Material>();
    public List<Material> m_pickableObstaclesMaterials = new List<Material>();
    public Material m_currentMaterial;
    int m_currentMaterialIndex = 0;

    public string m_powerupTag;
    public bool m_isStarPowerupOn = false;

    public int m_gunType = 1;
    public List<Sprite> m_gunsSprites;
    public Image m_gunIcon;
    public GameObject m_menuScreen;
    public GameObject m_gameplayScreen;
    public GameObject m_gameOverScreen;
    public GameObject m_reachedLevelScreen;
    public GameObject m_pauseScreen;
    public GameObject m_shopScreen;
    public GameObject m_p2pScreen;
    public GameObject m_pauseBtn;

    public Transform m_coinsContainer;
    public GameObject m_coinPrefab;

    public Image m_coinProgressBar;
    public TextMeshProUGUI m_coinProgressText;
    public Image m_gemProgressBar;
    public TextMeshProUGUI m_gemProgressText;

    public TextMeshProUGUI m_bonusText;
    public GameObject m_healthIcon;
    public GameObject m_starIcon;

    public GameObject m_soundBtn;

    public List<Sprite> m_soundSprites;

    public TextMeshProUGUI m_starPowerupCdText;

    public GameObject m_swipeIcon;

    private string m_gameplay_struct_path = "Gameplay";
    private TextAsset m_json;

    Gameplay_struct m_gameplay_struct;

    public Gameplay_level_struct m_currentLeveStruct;

    public int m_currentLevel = 1;
    public bool m_reset = false;

    public bool m_isCharacterBlinking = false;

    ////////////// CAMERA SHAKING EFFECT /////////////////

    Transform m_camTransform;
	
	// How long the object should shake for.
	float m_shakeDuration = 0f;
	
	// Amplitude of the shake. A larger value shakes the camera harder.
	float m_shakeAmount = 0.7f;
	float m_decreaseFactor = 1.0f;

	Vector3 m_originalPos;

   //////////////////////////////////////////////////////

    void Awake() 
    {
        // timer = time * 60f;
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        // LoadGamePlay
        LoadGamePlay();
        // Camera Shake
        m_camTransform = Camera.main.transform;
    }

    void OnEnable()
	{
		m_originalPos = m_camTransform.localPosition;
	}

    void LoadGamePlay()
    {
        if(!PlayerPrefs.HasKey("gameplay") || m_reset)
        {
            string gameplayPath = m_gameplay_struct_path + Path.DirectorySeparatorChar + "GameplayData";

            TextAsset json = Resources.Load(gameplayPath) as TextAsset;

            if (json != null)
            {
                m_gameplay_struct = JsonUtility.FromJson<Gameplay_struct>(json.text);
                PlayerPrefs.SetString("gameplay", json.text);
            }
            else
            {
                //Debug.LogError("Cannot find file: " + modulePath);
            }

            if(m_reset)
            {
                ResetAll();
            }

        }
        else
        {
            m_gameplay_struct = JsonUtility.FromJson<Gameplay_struct>(PlayerPrefs.GetString("gameplay"));
        }
        
        //  Define presets
        DefineGameplayPresets();

    }

    void ResetAll()
    {
        PlayerPrefs.SetInt("currentLevel",1);
        PlayerPrefs.SetInt("coins", 0);
        PlayerPrefs.SetInt("gems", 0);
        PlayerPrefs.SetInt("guntype", 1);
    }

    void DefineGameplayPresets()
    {
        //  Define props
        m_currentLevel = PlayerPrefs.GetInt("currentLevel",1);
        m_currentLeveStruct = m_gameplay_struct.levels[(m_currentLevel-1)]; // index start from 0
        m_cylindreRotSpeed = (m_currentLeveStruct.ringsSpeed * m_BASE_ROTATION_SPEED);
        m_minObstacles = m_currentLeveStruct.minObstacles;
        m_maxObstacles = m_currentLeveStruct.maxObstacles;
        m_ringsSpacing = m_currentLeveStruct.ringsSpacing;
        m_maxColors    = m_currentLeveStruct.colors;

        GetRandomNumberOfColours(m_maxColors);

        // Define SelectedColorsBox
        DefineSelectionColorsBox();

        // Define Gun Type
        m_gunType = PlayerPrefs.GetInt("gunType", 1);

    }

    void DisplaySwipeIcon()
    {
        if(PlayerPrefs.GetString("swiped", "false") == "false")
        {
            StartCoroutine(IDisplaySwipeIcon());
        }
    }
    IEnumerator IDisplaySwipeIcon()
    {
        yield return new WaitForSeconds(1);
        m_swipeIcon.SetActive(true);
        yield return new WaitForSeconds(3);
        m_swipeIcon.SetActive(false);
        PlayerPrefs.SetString("swiped", "true");

    }
    public void SetGunType(int gt)
    {
        m_gunType = gt;
        PlayerPrefs.SetInt("guntype", gt);
    }

    void DefineSelectionColorsBox()
    {
        m_selectionColorsBox.transform.GetComponent<SelectedColorsCtrlr>().DefineBoxes(m_pickableObstaclesMaterials);
    }
    public bool IsGameOver()
    {
        return m_gameOver;
    }
    public bool IsGamePaused()
    {
        return m_gamePaused;
    }

    public void StartGameplay()
    {

        m_menuScreen.SetActive(false);
        m_gameOverScreen.SetActive(false);
        m_gameplayScreen.SetActive(true);
       
        int soundInt = PlayerPrefs.GetInt("sound", 1);
        m_audioSource.gameObject.SetActive(soundInt == 1);

        // GunType
        m_gunIcon.sprite = m_gunsSprites[m_gunType-1];

        // Check Levelup
        CheckLevelUp();

    }

    public void Go()
    {
        m_gameOver = false;
        m_gamePaused = false;

        // Reset
        PlayerPrefs.SetString("restart", "false");

        // int soundInt = PlayerPrefs.GetInt("sound", 0);
        // m_audioSource.gameObject.SetActive(soundInt == 1);
        
        // Swipe intro
        DisplaySwipeIcon();

    }

    public void Restart(string restarting = "true")
    {
        PlayerPrefs.SetString("restart", restarting);
        Destroy(gameObject);
        SceneManager.LoadScene("Gameplay");
        //PlayerPrefs.SetString("restart", "false");

    }
    void Start()
    {
        ToggleSound();

        SetCurrentMaterial(0); // Red

        // Toggle screens
        m_menuScreen.SetActive(true);
        m_gameOverScreen.SetActive(false);
        m_gameplayScreen.SetActive(false);

        ChangeBulletColor();

        // Pause sound
        m_audioSource.gameObject.SetActive(false);

        if(PlayerPrefs.GetString("restart", "false") == "true")
        {
            StartGameplay();
            // Debug.Log("restarted");
        }

        // Check level up
        CheckLevelUp(); // Defining too stats

    }
 
    // This must get (max - (currentNumberOfColors)) without selecting a existing one
    void GetRandomNumberOfColours(int max)
    {
        int maxColorsToPick = max - m_pickableObstaclesMaterials.Count;

        if(maxColorsToPick > 0)
        {

            int counter = 0;
    
            while(counter < maxColorsToPick)
            {
                int randomColourIndex = Random.Range(0, m_obstaclesMaterials.Count);
                Material mat = m_obstaclesMaterials[randomColourIndex];
                if(m_pickableObstaclesMaterials.IndexOf(mat) == -1)
                {
                    m_pickableObstaclesMaterials.Add(mat);
                    counter++;
                }
            }
        
        }

        // return selectedMaterials;

    }
    void SetCurrentMaterial(int index)
    {
        m_currentMaterial = m_pickableObstaclesMaterials[index]; // Red
        m_currentMaterialIndex = index;
    }
    public void ChangeBulletColor()
    {
        int nextMaterialIndex = m_currentMaterialIndex + 1;
        if(nextMaterialIndex == m_pickableObstaclesMaterials.Count)
            nextMaterialIndex = 0;
        Debug.Log(m_pickableObstaclesMaterials.Count);
        SetCurrentMaterial(nextMaterialIndex);
        m_selectionColorsBox.transform.GetComponent<SelectedColorsCtrlr>().ActivateColor(nextMaterialIndex);

    }


    void CheckLevelUp()
    {

        int coins = m_characterCtrlr.m_coins;
        int gems = m_characterCtrlr.m_gems;
        int coinsBase = 1000;
        int gemsBase  = 100;
        if(m_currentLevel == 1) 
        {
            coinsBase = 300;
            gemsBase  = 20;
        }
        int thisLevelPassCoins = m_currentLevel * coinsBase;
        int thisLevelPassGems = m_currentLevel * gemsBase;

        // Progression -------

        float coinLevelPercentage = (float)coins / (float)thisLevelPassCoins;
        float gemLevelPercentage = (float)gems / (float)thisLevelPassGems;
        
        m_coinProgressBar.transform.GetComponent<Image>().fillAmount = coinLevelPercentage;
        m_gemProgressBar.transform.GetComponent<Image>().fillAmount = gemLevelPercentage;

        m_coinProgressText.text = coins + " / " + thisLevelPassCoins;
        m_gemProgressText.text = gems + " / " + thisLevelPassGems;

        // -------------------

        if((coins >= thisLevelPassCoins) && (gems >= thisLevelPassGems))
        {
            UnlockNextLevel();
        }

    }

    public void GetCoin(int type, int point)
    {
        // Effects
        if(point > 0) // Points type : Else heart
        {
            m_bonusText.text = "+" + point;
            if(m_bonusText.gameObject.activeInHierarchy)
                m_bonusText.gameObject.SetActive(false);
            m_bonusText.gameObject.SetActive(true);
        }
        else
        {
            if(type == 3) // Heart
            {        
                if(m_healthIcon.gameObject.activeInHierarchy)
                    m_healthIcon.gameObject.SetActive(false);
                m_healthIcon.gameObject.SetActive(true);
            }
            else if(type == 4) // Star
            {
                if(m_starIcon.gameObject.activeInHierarchy)
                    m_starIcon.gameObject.SetActive(false);
                m_starIcon.gameObject.SetActive(true);
            }
        }

        m_characterCtrlr.GetCoin(type, point);

        CheckLevelUp();
    }

    public void ToggleMotionOnInjured(bool status)
    {
        m_gamePaused = status;
    }
    public void UnlockNextLevel()
    {
        // m_gameOver = true;
        // m_gamePaused = true;

        if((m_currentLevel + 1) < m_gameplay_struct.levels.Count)
        {
            // Setup next level presets
            m_currentLevel++;
            PlayerPrefs.SetInt("currentLevel", (m_currentLevel));
            // Unlock Next Config

            // Goto Congrats Screen
            StartCoroutine(IDisplayCongrats());

            // Define gameplay preset
            DefineGameplayPresets();
        }
        else
        {

        }
     
    }

    void DisplayCongrats(bool status)
    {
        m_reachedLevelScreen.SetActive(status);
    }

    IEnumerator IDisplayCongrats()
    {
        // Update Level
        m_characterCtrlr.NextLevel((m_currentLevel));

        DisplayCongrats(true);

        m_audioSource.PlayOneShot(m_levelupSound);

        yield return new WaitForSeconds(1.0f);
        DisplayCongrats(false);
    }
    public void DecreasePlayerLives()
    {
        // m_shakeDuration = 2.0f;
        m_characterCtrlr.DecreasePlayerLives();
    }


    public void StartStarPowerup()
    {
        // Get current ball material (V)
        // Get all obstacles (by script) and set their new mat and tag
        // Display starPowerupCountDown et Start countdown
        
        m_isStarPowerupOn = true;

        m_powerupTag = m_currentMaterial.name;
        string[] splitArray =  m_powerupTag.Split(char.Parse("_")); //
        m_powerupTag = splitArray[1];

        ObstacleCtrlr[] allObstacles = FindObjectsOfType<ObstacleCtrlr>();
        foreach (ObstacleCtrlr obst in allObstacles)
        {
            obst.Customize(m_powerupTag, m_currentMaterial);
        }

        // Display Countdown and Start It
        StartStarPowerupCountdown();

        // Start Waiting
        StartCoroutine(IWaitBeforFinishingStarPowerup());
        
    }

    IEnumerator IWaitBeforFinishingStarPowerup()
    {
        yield return new WaitForSeconds(11) ;// seconds
        m_isStarPowerupOn = false;
        ObstacleCtrlr[] allObstacles = FindObjectsOfType<ObstacleCtrlr>();
        foreach (ObstacleCtrlr obst in allObstacles)
        {
            obst.ResetObstacle();
        }

    }

    void StartStarPowerupCountdown()
    {
        m_starPowerupCdText.gameObject.SetActive(true);
        int count = 10;
        StartCoroutine(CountDown(count));
    }

    IEnumerator CountDown(int _countdown)
    {   
        float cdDuration = 1f;
        m_starPowerupCdText.text = _countdown.ToString();
        yield return new WaitForSeconds(cdDuration); 
        _countdown--;
        if(_countdown > 0)
        {
            StartCoroutine(CountDown(_countdown));
        }
        else
        {
            m_starPowerupCdText.text = _countdown.ToString();
            yield return new WaitForSeconds(0.25f);
            m_starPowerupCdText.gameObject.SetActive(false);

        }
    }
    public void GameOver()
    {
         
        m_audioSource.PlayOneShot(m_gameoverSound);
        StartCoroutine(IWaitBeforeStopingSound());
        m_gameOver = true;
        m_gamePaused = true;

        // Toggle screens
        m_menuScreen.SetActive(false);
        m_gameOverScreen.SetActive(true);
        m_gameplayScreen.SetActive(false);
    }

    public void GotoMenu()
    {
        // PlayerPrefs.SetString("restart", "false");
        // SceneManager.LoadScene("Gameplay");
        // m_menuScreen.SetActive(false);

        Restart("false");

    }

    public void GotoShop()
    {
        m_menuScreen.SetActive(false);
        m_shopScreen.SetActive(true);

        // m_shopScreen.transform.GetComponent<ShopScreenCtrlr>().SetupUI();
    }

    public void FromShopToMenu()
    {
        m_menuScreen.SetActive(true);
        m_shopScreen.SetActive(false);
    }
    public void ToggleP2P(bool status)
    {
        m_menuScreen.SetActive(!status);
        m_p2pScreen.SetActive(status);
    }

    IEnumerator IWaitBeforeStopingSound()
    {
        yield return new WaitForSeconds(1.5f);
        m_audioSource.gameObject.SetActive(false);
    }
   
    public void PauseGame()
    {
        m_gameOver = false;
        m_gamePaused = true;
        m_audioSource.gameObject.SetActive(false);
        m_pauseBtn.SetActive(false);

        // Toggle screens
        m_pauseScreen.SetActive(true); 
    }
    public void ResumeGame()
    {
        m_gameOver = false;
        m_gamePaused = false;
        
        if(PlayerPrefs.GetInt("sound", 0) == 1)
            m_audioSource.gameObject.SetActive(true);
        m_pauseBtn.SetActive(true);

        // Toggle screens
        m_pauseScreen.SetActive(false); 
    }

    public void ToggleSound(bool autoToggle = true)
    {
        int soundState = PlayerPrefs.GetInt("sound", 1);
        if(!autoToggle)
        {
            soundState = 1 - soundState; // 1--> 0; 0--> 1
        }
        PlayerPrefs.SetInt("sound", soundState);
        m_soundBtn.GetComponent<Image>().sprite = m_soundSprites[soundState];
    }
}



[System.Serializable]
public class Gameplay_prop_struct
{
    public int prop = 25;
}
[System.Serializable]
public class Gameplay_level_struct
{
    public int levelId = 1;

    public int minObstacles = 1;
    public int maxObstacles = 1;
    public int colors = 2;
    public int ringsSpacing = 2;
    public int ringsSpeed = 1;
    public List<Gameplay_prop_struct> proports;

}

[System.Serializable]
public class Gameplay_struct
{
    public List<Gameplay_level_struct> levels;
}

